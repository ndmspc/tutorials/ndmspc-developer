import {AppGlobalScope, Distributor} from "@ndmspc/react-ndmspc-core";

// create instance of this global scope
const appGlobalScope = new AppGlobalScope({
    bins: new Map(),
    color: null
})

// add subjects to broker
appGlobalScope.addDistributor(new Distributor("hover"));
appGlobalScope.addDistributor(new Distributor("colorChange"));
appGlobalScope.addDistributor(new Distributor("binSelect"));
appGlobalScope.addDistributor(new Distributor("click"));

// input function to modify incoming data and set global scope states
const handleInputBinSelectFunc = (binData) => {
    if (binData) {
        const selectedBin = appGlobalScope.state.bins.get(binData.id);
        if (selectedBin) {
            appGlobalScope.state.bins.delete(binData.id);
        } else {
            appGlobalScope.state.bins.set(binData.id, binData.data);
        }
        appGlobalScope
            .getDistributorById("binSelect")
            ?.sendOutputData(Array.from(appGlobalScope.state.bins.keys()));
    }
};

// input function to modify incoming data and set global scope states
const handleInputHoverFunc = (binData) => {
    if (binData) {
        appGlobalScope
            .getDistributorById("hover")
            ?.sendOutputData(binData);
    }
};

// input function to modify incoming data and set global scope states
const handleInputClickFunc = (binData) => {
    if (binData) {
        appGlobalScope
            .getDistributorById("click")
            ?.sendOutputData(binData);
    }
};

// input function to modify incoming data and set global scope states
const handleInputColorChangeFunc = (color) => {
    if (color) {
        console.log(color);
        // save received color into global state
        appGlobalScope.state.color = color
        // publish received color to external components
        appGlobalScope
            .getDistributorById("colorChange")
            ?.sendOutputData(color);
    }
};

appGlobalScope.addHandlerFunc(
    "click",
    handleInputClickFunc,
    null
);

appGlobalScope.addHandlerFunc(
    "binSelect",
    handleInputBinSelectFunc,
    null
);

appGlobalScope.addHandlerFunc(
    "hover",
    handleInputHoverFunc,
    null
);

appGlobalScope.addHandlerFunc(
    "colorChange",
    handleInputColorChangeFunc,
    null
);

// set subscriptions
appGlobalScope.addAllSubscriptions();


// handler for processing data from component
const handleClick = (data) => {
    appGlobalScope.getDistributorById("click")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}

const handleHover = (data) => {
    appGlobalScope.getDistributorById("hover")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}

const handleDbClick = (data) => {
    appGlobalScope.getDistributorById("binSelect")?.sendInputData({
        id: `${data.id}-${data.binx}-${data.biny}`,
        data: data
    })
}


export {appGlobalScope, handleHover, handleDbClick, handleClick}
