import { createHistogram } from "@ndmspc/react-ndmspc-core";

function getRandomInt() {
    return Math.floor(Math.random() * 10);
}

const createTh2Histogram = () => {
    const histo = createHistogram('TH2I', 6, 6)
    for (let iy=1; iy<=6; iy++)
        for (let ix=1; ix<=6; ix++) {
            let bin = histo.getBin(ix, iy)
            histo.setBinContent(bin, getRandomInt());
        }
    histo.fXaxis.fTitle = 'x Axis'
    histo.fYaxis.fTitle = 'y Axis'
    histo.fName = "example"
    histo.fTitle = "title"
    histo.fMaximum = 10

    return histo
}

export { createTh2Histogram }