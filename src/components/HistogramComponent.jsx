import {NdmVrScene} from "@ndmspc/ndmvr";
import {appGlobalScope, handleClick, handleDbClick, handleHover} from "../AppGlobalContext.js";
import {useEffect, useState} from "react";


const HistogramComponent = ({ histogram }) => {
    const [bins, setBins] = useState(Array.from(appGlobalScope.state.bins.keys()))
    const [color, setColor] = useState(appGlobalScope.state.color)

    const handleBinColor = (data) => {
        let finalColor = color
        bins?.forEach((x) => {
            if (x === `${data.histogramId}-${data.x}-${data.y}`) {
                finalColor = 'red'
            }
        })
        return finalColor
    }

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'binSelect',
            (data) => { setBins(data) },
            null
        )
        return () => subscription.unsubscribe()
    })

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'colorChange',
            (data) => { setColor(data) },
            null
        )
        return () => subscription.unsubscribe()
    })

    return <NdmVrScene
        data={{
            histogram: histogram,
            id: 'example',
            projectionsNames: [],
            projPanelIds: [],
        }}
        onDbClick={handleDbClick}
        onClick={handleClick}
        onHover={handleHover}
        onView={handleBinColor}
    />
}

export default HistogramComponent