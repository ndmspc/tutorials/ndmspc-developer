import "../styles.css"
import {useEffect, useState} from "react";
import {appGlobalScope} from "../AppGlobalContext.js";

const ColorChangeComponent = () => {
    // color state of the component
    const [color, setColor] = useState('yellow');

    const handleChange = (event) => {
        setColor(event.target.value)
    }

    const handleSetColor = (config) => {
        appGlobalScope.getDistributorById('colorChange').sendInputData(color)
    }

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'colorChange',
            (currentColor) => { setColor(currentColor) },
            null
        )
        return () => subscription.unsubscribe()
    }, [])

    return <div className="panel">
        <h2>Color configurator for Component 1 histogram</h2>
        <div className="line">
            <input className="color-input" type="color" value={color} onChange={handleChange}/>
            <button className="confirm-button" onClick={handleSetColor}>Set color</button>
        </div>
    </div>
}

export default ColorChangeComponent