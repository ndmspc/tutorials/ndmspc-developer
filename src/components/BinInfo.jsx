import {useEffect, useState} from "react";
import '../styles.css'
import {appGlobalScope} from "../AppGlobalContext.js";

const BinInfo = () => {
    const [binInfo, setBinInfo] = useState(null)
    const [selectedCount, setSelectedCount] = useState(0)

    const handleDisplayingProjection = (binData) => {
        setBinInfo(binData);
    }

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'hover',
            handleDisplayingProjection,
            null
        )
        return () => subscription.unsubscribe()
    }, [])

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'binSelect',
            (data) => { setSelectedCount(data.length) },
            null
        )
        return () => subscription.unsubscribe()
    }, [])

    return <div className="panel">
        <h2>Selected bin for histogram {binInfo?.data?.id}</h2>
        <p className="line x-axis"><b>x axis: </b>{binInfo?.data?.binx}</p>
        <p className="line y-axis"><b>y axis: </b>{binInfo?.data?.biny}</p>
        <p className="line"><b>intersection: </b>{binInfo?.data?.intersectionAxis}</p>
        <p className="line"><b>content: </b>{binInfo?.data?.con}</p>
        <p className="line"><b>selected bin count: </b>{selectedCount ? selectedCount : 0}</p>
    </div>
}

export default BinInfo