import "../styles.css"
import {appGlobalScope} from "../AppGlobalContext.js";
import {createTH1Projection, displayImageOfProjection} from "@ndmspc/react-ndmspc-core";
import {useEffect} from "react";

const ProjectionComponents = () => {

    const handleCreateProjection = async (data) => {
        if (data) {
            await createTH1Projection(data.data.intersectionAxis, data.data, ['projection'], ['X', 'Y'])
            await displayImageOfProjection('projection', ['panel_0'], '800px', '800px')
        }
    }

    useEffect(() => {
        const subscription = appGlobalScope.createExternalSubscription(
            'click',
            handleCreateProjection,
            null
        )
        return () => subscription.unsubscribe()
    }, [])

    return <div className="proj-panel" id="projection" />
}

export default ProjectionComponents