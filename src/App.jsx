import HistogramComponent from "./components/HistogramComponent.jsx";
import BinInfo from "./components/BinInfo.jsx";
import './styles.css'
import ColorChangeComponent from "./components/ColorChangeComponent.jsx";
import {createTh2Histogram} from "./utils/utils.js";
import ProjectionComponent from "./components/ProjectionComponent";

const App = () => {

    const histogram = createTh2Histogram()

    return (
        <div className="main">
            <div className="main-section">
                <HistogramComponent histogram={histogram} />
            </div>
            <div className="aside-section">
                <BinInfo />
                <ColorChangeComponent />
                <ProjectionComponent histogram={histogram} />
            </div>
        </div>
    )
}

export default App
